import os
import cv2
import numpy as np
import csv
import pandas
import time
from multiprocessing import Process, Queue

from tools.object_detection import algorithm_detection
from tools.object_tracker import define_deep_sort, algorithm_tracker, non_max_suppression_tracker
from tools.counting import create_csv, fill_csv
from tools.roi import line, roi
from tools.gui import draw_roi, draw_box_objet, draw_count

from absl import app, flags
from absl.flags import FLAGS

import tensorflow as tf
from tensorflow.compat.v1 import ConfigProto
from tensorflow.python.saved_model import tag_constants

# Comente debajo de la linea para habilitar las salidas de registro de TensorFlow
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# Asignar GPU
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

flags.DEFINE_string('weights', './data/checkpoints/yolov4-tiny-416-salmon', 'Ruta al archivo de pesos')
flags.DEFINE_integer('size', 416, 'Tamaño de las imágenes escaladas')
flags.DEFINE_string('video', './data/videos/Video_Salmones_Demo_1080.mp4', 'Ruta del video de entrada')
flags.DEFINE_string('output', 'outputs/output.avi', 'Ruta de salida del video procesado')
flags.DEFINE_string('csv', 'salmones.csv', 'Nombre del archivo .csv')
flags.DEFINE_boolean('dont_show', False, 'No mostrar salida de videos')
flags.DEFINE_float('iou', 0.2, 'Umbral IoU')
flags.DEFINE_float('score', 0.8, 'Umbral de confianza de detección')
flags.DEFINE_float('matching_threshold', 0.8, 'Umbral de la no similitud máxima para que se considere el mismo objeto')
flags.DEFINE_float('overlap', 1, 'Define el porcentaje de solapamiento en el tracking')
flags.DEFINE_integer('max_age', 10, 'Cantidad de veces que se pierde la detección de un objeto antes de ser eliminado')
flags.DEFINE_float('px', 1 / 2, 'Punto central, valor x')
flags.DEFINE_float('py', 1 / 2, 'Punto central, valor y')
flags.DEFINE_float('angle', 30, 'Inclinación recta región de interés (RoI)')
flags.DEFINE_integer('separation', 40, 'Tamaño de la Región de interés (RoI)')


def main(_argv):
    # Configuración de carga de la GPU
    config = ConfigProto()
    config.gpu_options.allow_growth = True

    # Cargar el modelo de pesos de YOLO y definir Deep SORT
    saved_model_loaded = tf.saved_model.load(FLAGS.weights, tags=[tag_constants.SERVING])
    infer = saved_model_loaded.signatures['serving_default']
    tracker, encoder = define_deep_sort(FLAGS.matching_threshold, FLAGS.max_age)

    # Llamar video
    cap = cv2.VideoCapture(FLAGS.video)

    # Fecha de modificación del video
    video_date = os.path.getmtime(FLAGS.video)

    # Formato del video a grabar
    fps = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    codec = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(FLAGS.output, codec, fps, (width, height))

    # Definir región de interés RoI
    vars_line = (FLAGS.angle, FLAGS.px, FLAGS.py, width, height)
    roi_area = roi(FLAGS.separation, vars_line)
    roi_line = line(0, vars_line)

    # Definir el archivo .csv y sus variables
    output_csv = create_csv(FLAGS.csv)
    id_counter = set()
    old_accountant = 0
    old_total_count = 0
    minimum_coefficients = []
    maximum_coefficients = []
    average_coefficients = []
    frame_num = 0

    # Tiempos detección y rastreo
    tracker_times = []
    detection_times = []

    # Iniciar el proceso de conteo
    while cap.isOpened():
        start_time = time.time()
        # Extraer fotograma
        ret, frame = cap.read()
        frame_num += 1
        if not ret:
            print('El procesamiento de los fotogramas a finalizado.\n')
            break

        # Formato imagen
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        detection_start = time.time()  # Inicio proceso detección
        image_data = cv2.resize(frame, (FLAGS.size, FLAGS.size))
        image_data = image_data / 255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)

        # Llamar algoritmo de detección de objetos
        detection_frame = algorithm_detection(infer, image_data, height, width, FLAGS.iou, FLAGS.score)
        bboxes, scores, names, count_on_screen = detection_frame
        detection_times.append(time.time()-detection_start)
        print('Tiempo de detección: {}'.format(round(detection_times[-1], 3)))

        # Llamar algoritmo de seguimiento de objetos
        tracker_start = time.time()
        tacking_detections = algorithm_tracker(encoder, frame, bboxes, scores, names)
        non_max_suppression_detections = non_max_suppression_tracker(tacking_detections, FLAGS.overlap)

        # Actualizar rastreador
        tracker.predict()
        tracker.update(non_max_suppression_detections)
        tracker_times.append(time.time()-tracker_start)
        print('Tiempo de rastreo: {}'.format(round(tracker_times[-1], 3)))

        # Dibujar región de interes (RoI)
        frame = draw_roi(frame, width, height, FLAGS.px, FLAGS.py, roi_line, roi_area)

        # Dibujar objetos rastreados
        for track in tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue
            # class_name = track.get_class()
            class_name = 'Salmon'
            bbox = track.to_tlbr()

            # Centro del objeto rastreado
            center_x = int((bbox[0] + bbox[2]) / 2)
            center_y = int((bbox[1] + bbox[3]) / 2)
            center = (center_x, center_y)

            # Si el objeto entra al poligono, agregar el ID al contador
            inside_region = cv2.pointPolygonTest(roi_area, center, False)
            if inside_region > 0:
                id_counter.add(int(track.track_id))

            # Dibujar recuadro objeto
            frame = draw_box_objet(frame, bbox, track.track_id, class_name, center)

        # Conteo actual y total
        current_count = len(id_counter)
        total_count = old_total_count + current_count
        frame = draw_count(frame, total_count)

        # Datos archivo .csv
        try:
            minimum_coefficients.append(min(scores))
            maximum_coefficients.append(max(scores))
            average_coefficients.append(sum(scores) / len(scores))

        except ValueError:
            pass

        if frame_num % 30 == 0:
            video_date += 1
            fill_csv(output_csv, video_date, count_on_screen, old_accountant, total_count, minimum_coefficients,
                     maximum_coefficients, average_coefficients)

            old_accountant = total_count
            minimum_coefficients = []
            maximum_coefficients = []
            average_coefficients = []

        # Ajustar contador de IDs desfasado
        if frame_num % 50 == 0:
            outdated_id_counter = id_counter.copy()
            # print(outdated_id_counter)

        # Limpiar IDs del contador
        if frame_num % 90 == 0:
            # print(id_counter)
            id_counter = id_counter.copy() - outdated_id_counter.copy()
            # print(id_counter)
            old_total_count = total_count - len(id_counter)

        # Guardar y visualizar detección
        result = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        out.write(result)

        if not FLAGS.dont_show:
            cv2.imshow("Conteo de Salmones", result)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        fps = round(1.0 / (time.time() - start_time), 3)
        print('Frame #{}   FPS: {} \nObjetos que se están rastreando: {} \nTotal contado: {}\n'.format(
            frame_num, fps, count_on_screen, total_count))


    detection_times.pop(0)
    detection_times.pop(0)
    print('Tiempo de detección promedio: {}s'.format(round(sum(detection_times) / len(detection_times), 3)))
    print('Tiempo de rastreo promedio: {}s \n'.format(round(sum(tracker_times) / len(tracker_times), 3)))
    cap.release()
    out.release()
    cv2.destroyAllWindows()

    table = pandas.read_csv(output_csv)
    print(table)


if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
