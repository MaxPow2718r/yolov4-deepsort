import os
import cv2
import numpy as np
import csv
import pandas
import time
from multiprocessing import cpu_count, Process, Queue, Pipe
import queue

from tools.object_detection import algorithm_detection
from tools.object_tracker import define_deep_sort, algorithm_tracker, non_max_suppression_tracker
from tools.counting import create_csv, fill_csv
from tools.roi import line, roi
from tools.gui import draw_roi, draw_box_objet, draw_count

from absl import app, flags
from absl.flags import FLAGS

import tensorflow as tf
from tensorflow.compat.v1 import ConfigProto
from tensorflow.python.saved_model import tag_constants

# Comente debajo de la linea para habilitar las salidas de registro de TensorFlow
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

flags.DEFINE_string('weights', './data/checkpoints/yolov4-tiny-416-salmon', 'Ruta al archivo de pesos')
flags.DEFINE_integer('size', 416, 'Tamaño de las imágenes escaladas')
flags.DEFINE_string('video', './data/videos/Video_Salmones_Demo_1080.mp4', 'Ruta del video de entrada')
flags.DEFINE_string('output', None, 'Ruta de salida del video procesado')
flags.DEFINE_string('csv', 'salmones.csv', 'Nombre del archivo .csv')
flags.DEFINE_boolean('dont_show', False, 'No mostrar salida de videos')
flags.DEFINE_float('iou', 0.2, 'Umbral IoU')
flags.DEFINE_float('score', 0.8, 'Umbral de confianza de detección')
flags.DEFINE_float('matching_threshold', 0.8, 'Umbral de la no similitud máxima para que se considere el mismo objeto')
flags.DEFINE_float('overlap', 1, 'Define el porcentaje de solapamiento en el tracking')
flags.DEFINE_integer('max_age', 40, 'Cantidad de veces que se pierde la detección de un objeto antes de ser eliminado')
flags.DEFINE_float('px', 1 / 2, 'Punto central, valor x')
flags.DEFINE_float('py', 1 / 2, 'Punto central, valor y')
flags.DEFINE_float('angle', 30, 'Inclinación recta región de interés (RoI)')
flags.DEFINE_integer('separation', 40, 'Tamaño de la Región de interés (RoI)')


def object_detection(rx_images_data, tx_predicted_data, tx_processing_times, long_frames, height, width, weights,
                     iou,
                     score):
    # Asignar GPU
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    if len(physical_devices) > 0:
        tf.config.experimental.set_memory_growth(physical_devices[0], True)

    # Configuración de carga de la GPU
    config = ConfigProto()
    config.gpu_options.allow_growth = True
    # config.gpu_options.per_process_gpu_memory_fraction = 0.4

    # Cargar el modelo de pesos de YOLO
    saved_model_loaded = tf.saved_model.load(weights, tags=[tag_constants.SERVING])
    infer = saved_model_loaded.signatures['serving_default']

    count_frames = 0
    while count_frames < long_frames:
        if count_frames % 30 == 0:
            tx_processing_times.send(time.time())
        count_frames += 1

        image_data = rx_images_data.recv()

        # Llamar algoritmo de detección de objetos
        detection_frame = algorithm_detection(infer, image_data, height, width, iou, score)

        tx_predicted_data.send(detection_frame)


def object_tracker(rx1_frames_data, rx_predicted_data, tx_tracker_data, long_frames, overlap, matching_threshold,
                   max_age):
    # definir Deep Sort
    tracker, encoder = define_deep_sort(matching_threshold, max_age)

    count_frames = 0
    while count_frames < long_frames:
        count_frames += 1

        image_data = rx1_frames_data.recv()

        # Llamar algoritmo de seguimiento de objetos
        bboxes, scores, names, count_on_screen = rx_predicted_data.recv()
        # start = time.time()
        print('Frame #{}. Objetos que se están rastreando: {}\n'.format(count_frames, count_on_screen))
        tacking_detections = algorithm_tracker(encoder, image_data, bboxes, scores, names)
        non_max_suppression_detections = non_max_suppression_tracker(tacking_detections, overlap)
        # print("Tiempo de rastreo: {}s".format(round(time.time() - start, 4)))

        tx_tracker_data.send(non_max_suppression_detections)


def update_tracking(rx2_frames_data, tx_processed_frames, rx_tracker_data, long_frames,
                    matching_threshold, max_age):
    # definir Deep Sort
    tracker, encoder = define_deep_sort(matching_threshold, max_age)

    count_frames = 0
    while count_frames < long_frames:
        count_frames += 1
        tacking_detections = rx_tracker_data.recv()
        image_data = rx2_frames_data.recv()

        # Actualizar rastreador
        tracker.predict()
        tracker.update(tacking_detections)

        # tracked_bboxes = []
        for track in tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue
            bbox = track.to_tlbr()
            tracking_id = track.track_id
            # class_name = track.get_class()
            class_name = 'Salmon'

            # Centro del objeto rastreado
            center_x = int((bbox[0] + bbox[2]) / 2)
            center_y = int((bbox[1] + bbox[3]) / 2)
            center = (center_x, center_y)

            image_data = draw_box_objet(image_data, bbox, tracking_id, class_name, center)
        tx_processed_frames.send(image_data)


def write_video(rx_processed_frames, rx_processing_times, output, fourcc, fps, dim, long_frames):
    out = cv2.VideoWriter(output, fourcc, fps, dim)
    count_frames = 0
    while count_frames < long_frames:
        count_frames += 1

        frame = rx_processed_frames.recv()
        out.write(frame)

        if count_frames % 30 == 0:
            print('FPS: {}'.format(round(30 / (time.time() - rx_processing_times.recv()), 2)))

    out.release()


def main(_argv):
    # Llamar video
    cap = cv2.VideoCapture(FLAGS.video)

    # Cantidad de fotogramas
    long_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # Formato del video a grabar
    fps = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    codec = cv2.VideoWriter_fourcc(*'XVID')

    # tx_images_data_even, rx_images_data_even = Pipe()
    # tx_images_data_odd, rx_images_data_odd = Pipe()
    tx_images_data, rx_images_data = Pipe()
    # tx1_frames_data_even, rx1_frames_data_even = Pipe()
    # tx1_frames_data_odd, rx1_frames_data_odd = Pipe()
    tx1_frames_data, rx1_frames_data = Pipe()
    tx2_frames_data, rx2_frames_data = Pipe()
    tx_predicted_data, rx_predicted_data = Pipe()
    # tx_predicted_data_even, rx_predicted_data_even = Pipe()
    # tx_predicted_data_odd, rx_predicted_data_odd = Pipe()
    # tx_processing_times_even, rx_processing_times_even = Pipe()
    # tx_processing_times_odd, rx_processing_times_odd = Pipe()
    tx_processing_times, rx_processing_times = Pipe()
    tx_tracker_data, rx_tracker_data = Pipe()
    # tx_tracker_data_even, rx_tracker_data_even = Pipe()
    # tx_tracker_data_odd, rx_tracker_data_odd = Pipe()
    tx_processed_frames, rx_processed_frames = Pipe()

    # p1_even = Process(target=object_detection, args=(rx_images_data_even, tx_predicted_data_even, tx_processing_times_even, True, long_frames, height, width, FLAGS.weights, FLAGS.iou, FLAGS.score))
    # p1_odd = Process(target=object_detection, args=(rx_images_data_odd, tx_predicted_data_odd, tx_processing_times_odd, False, long_frames, height, width, FLAGS.weights, FLAGS.iou, FLAGS.score))
    p1 = Process(target=object_detection, args=(rx_images_data, tx_predicted_data, tx_processing_times, long_frames, height, width, FLAGS.weights, FLAGS.iou, FLAGS.score))
    # p2_even = Process(target=object_tracker, args=(rx1_frames_data_even, rx_predicted_data_even, tx_tracker_data_even, long_frames, FLAGS.overlap, FLAGS.matching_threshold, FLAGS.max_age))
    # p2_odd = Process(target=object_tracker, args=(rx1_frames_data_odd, rx_predicted_data_odd, tx_tracker_data_odd, long_frames, FLAGS.overlap, FLAGS.matching_threshold, FLAGS.max_age))
    p2 = Process(target=object_tracker, args=(rx1_frames_data, rx_predicted_data, tx_tracker_data, long_frames, FLAGS.overlap, FLAGS.matching_threshold, FLAGS.max_age))
    p3 = Process(target=update_tracking, args=(
        rx2_frames_data, tx_processed_frames, rx_tracker_data, long_frames, FLAGS.matching_threshold, FLAGS.max_age))
    p4 = Process(target=write_video, args=(
        rx_processed_frames, rx_processing_times, FLAGS.output, codec, fps, (width, height), long_frames))
    # p1_even.start()
    # p1_odd.start()
    p1.start()
    # p2_even.start()
    # p2_odd.start()
    p2.start()
    p3.start()
    p4.start()

    start_time = time.time()
    # Iniciar captura de fotogramas
    count_frame = 0
    while cap.isOpened():
        ret, frame = cap.read()
        # print('Capturada #', count_frame)
        if not ret:
            print('Captura de fotogramas finalizado\n')
            break
        count_frame += 1

        # Formato imagen YOLO
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image_data = cv2.resize(frame, (FLAGS.size, FLAGS.size))
        image_data = image_data/255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)

        # if count_frame % 2 == 0:
        #     tx1_frames_data_even.send(frame)
        # else:
        #     tx1_frames_data_odd.send(frame)

        tx_images_data.send(image_data)
        tx1_frames_data.send(frame)
        tx2_frames_data.send(frame)

    cap.release()

    # p1_even.join()
    # p1_odd.join()
    p1.join()
    # p2_even.join()
    # p2_odd.join()
    p2.join()
    p3.join()
    p4.join()

    print('Tiempo de ejecución: {}'.format(round(time.time() - start_time, 2)))


if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
