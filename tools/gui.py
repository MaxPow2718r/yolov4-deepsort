import cv2

# Dibujar región de interes (RoI)
def draw_roi(frame, width, height, px, py, roi_line, roi_area):
    # Lineas de los ejes cartesianos
    cv2.line(frame, (0, int(height * py)), (width, int(height * py)), (0, 0, 0), thickness=1)
    cv2.line(frame, (int(width * px), 0), (int(width * px), height), (0, 0, 0), thickness=1)

    # Dibujar RoI
    cv2.line(frame, roi_line[0], roi_line[1], (0, 0, 255), thickness=6)
    cv2.polylines(frame, [roi_area], True, (255, 0, 0), 2)
    return frame

# Dibujar recuadro objeto
def draw_box_objet(frame, bbox, track_id, class_name, center):
    color = (255, 0, 255)
    cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
    cv2.circle(frame, center, 4, color, -1)
    cv2.rectangle(frame, (int(bbox[0]), int(bbox[1] - 28)),
                  (int(bbox[0]) + (len(class_name) + len(str(track_id))) * 20, int(bbox[1])), color, -1)
    cv2.putText(frame, class_name + "-" + str(track_id), (int(bbox[0] + 5), int(bbox[1] - 5)), 2, 0.75,
                (255, 255, 255), 2)
    return frame

def draw_count(frame, total_count):
    cv2.rectangle(frame, (50, 60), (400, 110), (255, 255, 255), -1)
    cv2.putText(frame, "Total contado: " + str(total_count), (70, 97), 3, 1, (0, 0, 255), 2, cv2.LINE_AA)
    return frame
