import numpy as np
import time

from deep_sort import preprocessing, nn_matching, generate_detections as gdet
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker


def define_deep_sort(max_cosine_distance, max_age):
    nn_budget = None
    # Inicializar Deep Sort
    model_filename = './data/model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)
    # Calcular la metrica de la distancia del coseno 
    metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
    # Inicializar tracker
    tracker = Tracker(metric, max_age)

    return tracker, encoder


def algorithm_tracker(encoder, frame, bboxes, scores, names):
    # Codificar detecciones de YOLO y alimentar al rastreador
    features = encoder(frame, bboxes)
    detections = [Detection(bbox, score, class_name, feature) for bbox, score, class_name, feature in
                  zip(bboxes, scores, names, features)]

    return detections


def non_max_suppression_tracker(detections, nms_max_overlap):
    # Ejecutar supresion no maxima
    boxs = np.array([d.tlwh for d in detections])
    scores = np.array([d.confidence for d in detections])
    classes = np.array([d.class_name for d in detections])
    indices = preprocessing.non_max_suppression(boxs, classes, nms_max_overlap, scores)
    non_max_suppression_detections = [detections[i] for i in indices]

    return non_max_suppression_detections
