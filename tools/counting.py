import os
import csv
import time

fieldnames = ['Hora', 'Salmones en Pantalla', 'Conteo Parcial', 'Conteo Total',
              'Confianza Minima', 'Confianza Maxima', 'Confianza Promedio']


def create_csv(name_csv):
    output_csv = os.getcwd() + '/outputs/csv_content/' + name_csv

    with open(output_csv, mode='w', encoding='UTF8', newline='') as file:
        csv_file = csv.DictWriter(file, fieldnames=fieldnames)
        csv_file.writeheader()

    return output_csv


def fill_csv(output_csv, video_date, count_on_screen, old_accountant, total_count, minimum_coefficients, maximum_coefficients, average_coefficients):

    with open(output_csv, mode='a', encoding='UTF8', newline='') as file:
        csv_file = csv.DictWriter(file, fieldnames=fieldnames)

        try:
            partial = total_count - old_accountant
            minimum = round(min(minimum_coefficients), 3)
            maximum = round(max(maximum_coefficients), 3)
            average = round(sum(average_coefficients) / len(average_coefficients), 3)

        except ValueError:
            pass

        row = {'Hora': time.ctime(video_date).split()[3],
               'Salmones en Pantalla': count_on_screen,
               'Conteo Parcial': partial,
               'Conteo Total': total_count,
               'Confianza Minima': minimum,
               'Confianza Maxima': maximum,
               'Confianza Promedio': average}
        csv_file.writerow(row)
