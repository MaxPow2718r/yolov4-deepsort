import numpy as np


# Función para crear la recta de la región de interes (RoI)
def line(phase, vars_line):
    angle, px, py, width, height = vars_line
    center_width = px * width
    center_height = py * height
    if abs(angle) == 90:
        var = int(center_width - phase)
        x = [var, var]
        y = [height, 0]
    elif int(angle) == 0:
        var = int(center_height - phase)
        x = [0, width]
        y = [var, var]
    else:
        x = [0, width]
        y = []
        m = np.tan(int(angle) * np.pi / 180)
        for i in range(2):
            y.append(int(round(center_height - phase - (m * (x[i] - center_width)))))
    return list(zip(x, y))


# Función para definir las coordenadas de los puntos de las esquinas del polígono de la región de interes (RoI)
def roi(separation, vars_line):
    pts1 = line(separation / 2, vars_line)
    pts2 = line(-separation / 2, vars_line)
    region_roi = np.array([pts1[0], pts2[0], pts2[1], pts1[1]])  # RoI
    region_roi_point = np.ascontiguousarray(region_roi)  # Cambiar formato RoI
    return region_roi_point
