import tensorflow as tf
import cv2
import numpy as np
import time

import core.utils as utils
from core.config import cfg


def algorithm_detection(infer, image_data, h, w, iou, score):

    batch_data = tf.constant(image_data)
    pred_bbox = infer(batch_data)
    for key, value in pred_bbox.items():
        boxes = value[:, :, 0:4]
        pred_conf = value[:, :, 4:]

    boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
        boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
        scores=tf.reshape(pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
        max_output_size_per_class=50,
        max_total_size=50,
        iou_threshold=iou,
        score_threshold=score)

    # Convierta datos en matrices numerosas y corte los elementos no utilizados 
    num_objects = valid_detections.numpy()[0]
    bboxes = boxes.numpy()[0]
    bboxes = bboxes[0:int(num_objects)]
    scores = scores.numpy()[0]
    scores = scores[0:int(num_objects)]
    classes = classes.numpy()[0]
    # classes = classes[0:int(num_objects)]

    # Formato de cuadros delimitadores de normalizado ymin, xmin, ymax, xmax ---> xmin, ymin, width, height
    bboxes = utils.format_boxes(bboxes, h, w)

    # Leer todos los nombres de clase de la configuracion
    # class_names = utils.read_class_names(cfg.YOLO.CLASSES)

    # Recorrer objetos y usar el indice de clase para obtener el nombre de la clase 
    names = []
    for i in range(num_objects):
        # class_indx = int(classes[i])
        # class_name = class_names[class_indx]
        class_name = 'Salmon'
        names.append(class_name)
    names = np.array(names)
    count = len(names)

    return bboxes, scores, names, count
